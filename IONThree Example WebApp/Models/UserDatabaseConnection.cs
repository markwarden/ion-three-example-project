﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

/*

    Password Hashing Algorithm reference: https://web.archive.org/web/20170613014751/https://www.codeproject.com/Articles/1112382/Login-functionality-in-MVC 
     
*/

namespace IONThree.Models
{
    public class UserDatabaseConnection
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["IonThree"].ConnectionString;
        private SqlConnection con;
        private int maxSaltLength = 2048;

        public SqlConnection ConnectToDB()
        {
            con = new SqlConnection(connectionString);
            return con;
        }

        public int CheckIfUserExistsByEmail(string email)
        {
            SqlConnection con = ConnectToDB();
            SqlCommand command = new SqlCommand("SELECT UserEmail FROM [User] WHERE UserEmail=@email;", con);
            int success = 0;
            command.Parameters.AddWithValue("@email", email);
            if(email == "") { return 0; }
            con.Open();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                string result = "";
                if (reader.HasRows)
                {
                    reader.Read();
                    result += String.Format("{0}", reader.GetValue(0));
                    success++; 
                }
            }

            con.Close();
            return success;
        }

        public void InsertNewCustomer(string name, string password, string email)
        {
            SqlConnection con = ConnectToDB();

            byte[] salt = GetSalt(maxSaltLength);

            SqlParameter userNameParam = new SqlParameter("@name", SqlDbType.VarChar) { Value = name },
                         userEmailParam = new SqlParameter("@email", SqlDbType.VarChar) { Value = email },
                         userSaltParam = new SqlParameter("@salt", SqlDbType.VarBinary) { Value = salt },
                         userHashParam = new SqlParameter("@passwordhash", SqlDbType.NText) { Value = GetHashSHA512(password, email, salt) };

            SqlCommand cmd = new SqlCommand("INSERT INTO [User] (UserName, UserEmail, Password, Salt) VALUES (@name, @email, @passwordhash, @salt);", con);

            cmd.Parameters.Add(userNameParam);
            cmd.Parameters.Add(userEmailParam);
            cmd.Parameters.Add(userSaltParam);
            cmd.Parameters.Add(userHashParam);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public bool DeleteCustomer(string email)
        {
            SqlConnection con = ConnectToDB();
            SqlCommand command = new SqlCommand("DELETE FROM [User] WHERE UserEmail = @email;", con);
            if(email != null)
            {
                command.Parameters.AddWithValue("@email", email);
                con.Open();
                command.ExecuteNonQuery();
                con.Close();
                return true;
            }
            return false;
        }

        private static byte[] GetSalt(int saltLength)
        {
            byte[] salt = new byte[saltLength];

            using (RNGCryptoServiceProvider random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }
            return salt;
        }

        public bool ValidateUserPassword(string email, string password)
        {
            if(CheckIfUserExistsByEmail(email) > 0)
            {
                SqlConnection con = ConnectToDB();
                SqlCommand com = new SqlCommand("SELECT Salt FROM [User] WHERE UserEmail=@email;", con);
                com.Parameters.AddWithValue("@email", email);

                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                reader.Read();
                byte[] salt = (byte[]) reader.GetValue(0);

                string dbPass = GetEncodedPassword(email);
                if(dbPass != "" || dbPass != password)
                {
                    if (GetHashSHA512(password, email, salt) == dbPass)
                    {
                        return true;
                    }
                }
                con.Close();
            }
            return false;
        }

        public string GetEncodedPassword(string email)
        {
            SqlConnection con = ConnectToDB();
            SqlCommand command = new SqlCommand("SELECT Password FROM [User] WHERE UserEmail = @email;", con);
            string result = "";
            command.Parameters.AddWithValue("@email", email);

            con.Open();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if(reader.HasRows)
                {
                    reader.Read();
                    result = String.Format("{0}", reader.GetValue(0));
                }
            }

            con.Close();
            return result;
        }

        private static string GetHashSHA512(string password, string email, byte[] salt)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(password + email.ToLower());

            byte[] plainTextWithSaltBytes = new byte[plainTextBytes.Length + salt.Length];

            for(int i = 0; i < plainTextBytes.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainTextBytes[i];
            }

            for(int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[plainTextBytes.Length + i] = salt[i];
            }

            HashAlgorithm hash = new SHA512Managed();
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);
            byte[] hashWithSaltBytes = new byte[hashBytes.Length + salt.Length];

            for(int i = 0; i < hashBytes.Length; i++)
            {
                hashWithSaltBytes[i] = hashBytes[i];
            }

            for (int i = 0; i < salt.Length; i++)
            {
                hashWithSaltBytes[hashBytes.Length - 1 + i] = salt[i];
            }
            Console.WriteLine("This appears to be working?");
            return Convert.ToBase64String(hashWithSaltBytes);
        }

        public string GetUserNameFromEmail(string email)
        {
            SqlConnection con = ConnectToDB();
            SqlCommand command = new SqlCommand("SELECT UserName FROM [User] WHERE UserEmail = @email;", con);
            string result = "";
            command.Parameters.AddWithValue("@email", email);

            con.Open();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    result = String.Format("{0}", reader.GetValue(0));
                }
            }

            con.Close();

            return result;
        }

        public void UpdateName(string email, string name)
        {
            SqlConnection con = ConnectToDB();
            SqlCommand command = new SqlCommand("UPDATE [User] SET UserName = @name WHERE UserEmail = @email;", con);
            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@name", name);

            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }
        public void UpdateNotes(string email, string notes)
        {
            SqlConnection con = ConnectToDB();
            SqlCommand command = new SqlCommand("UPDATE [User] SET Notes = @notes WHERE UserEmail = @email;", con);
            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@notes", notes);

            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }
    }
}