﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace IONThree.Models
{
    public class Customer
    {
        private UserDatabaseConnection uDC = new UserDatabaseConnection();
        private string _Password, _CustomerName, _Email;
        public bool isLoggedOut = true;

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid email address. ")]
        [EmailAddress]
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        [Required]
        [StringLength(50, ErrorMessage = "Name must be shorter than 50 characters. ")]
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }

        [Required]
        [RegularExpression("^[A-Z,a-z,0-9]{8,64}$", ErrorMessage = "ERROR: Password must be between 8 and 64 characters long. ")]
        public string Password
        {
            get { return _Password; }

            set
            {
                _Password = value;
            }
        }

        [Range(typeof(bool), "false", "false", ErrorMessage = "This user already exists. Please log in or choose another email address to create an account. ")]
        public bool isDuplicate { get; set; } 

        [Range(typeof(bool), "false", "false", ErrorMessage = "No user exists for this email address. Please check for spelling errors or start a new account. ")]
        public bool doesNotExist { get; set; }

        [Range(typeof(bool), "false", "false", ErrorMessage = "It appears that you're already logged in! ")]
        public bool isLoggedIn
        {
            get
            {
                if (_Password != null && _Password.Length >= 8 && _Email.Length > 3 && isLoggedOut)
                {
                    isLoggedOut = !uDC.ValidateUserPassword(_Email, _Password);
                    return !isLoggedOut;
                }
                else if (!isLoggedOut)
                {
                    _CustomerName = CustomerName = uDC.GetUserNameFromEmail(_Email);
                    return true;
                }

                return false;
            }
        }

        [Range(typeof(bool), "true", "true", ErrorMessage = "A user does not exist for this email address. Please check your spelling or register for a new account. ")]
        public bool userExists
        {
            get
            {
                if (_Email != null)
                {
                    if (uDC.CheckIfUserExistsByEmail(_Email) > 0)
                    {
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }

        public bool DeleteMe()
        {
            return uDC.DeleteCustomer(_Email);
        }

        public bool CreateMe() 
        {
            if(_Email != null && _Password != null && _CustomerName != null)
            {
                if (uDC.CheckIfUserExistsByEmail(_Email) > 0)
                {
                    isDuplicate = true;
                    return false;
                }
                else
                {
                    uDC.InsertNewCustomer(_CustomerName, _Password, _Email);
                    return true;
                }
            }
            return false;
        }

        public string Notes { get; set; }

        public void UpdateInfo(string email, string name, string notes)
        {
            if(uDC.CheckIfUserExistsByEmail(email) == 1)
            {
                if(name != null)
                {
                    uDC.UpdateName(email, name);
                }
                if(notes != null)
                {
                    uDC.UpdateNotes(email, notes);
                }
            }
        }
    }
}
