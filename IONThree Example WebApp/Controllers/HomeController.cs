﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IONThree.Models;

namespace IONThree.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(Customer obj)
        {
            return View("Home", obj);
        }

        public ActionResult Home(Customer obj)
        {
            return View("Home", obj);
        }
    }
}