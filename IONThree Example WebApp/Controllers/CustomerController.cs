﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IONThree.Models;

namespace IONThree.Controllers
{
    public class CustomerBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpContextBase objContext = controllerContext.HttpContext;
            Customer obj = new Customer();

            obj.Email = objContext.Request.Form["txtCustomerEmail"];
            obj.Password = objContext.Request.Form["txtCustomerPassword"];
            obj.CustomerName = objContext.Request.Form["txtCustomerName"];

            return obj;
        }
    }

    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Load(Customer obj)
        {
            return View("Customer", obj);
        }

        public ActionResult Enter()
        {
            return View("Enter");
        }

        public ActionResult Submit(Customer obj)
        {
            if (!obj.isDuplicate)
            {
                obj.CreateMe();
                return View("Login");
            }
            else if (obj.isDuplicate)
            {
                return View("Enter");
            }
            else
            {
                return View("Enter");
            }
        }

        public ActionResult Attempt(Customer obj)
        {
            if(!obj.isLoggedIn)
            {
                return View("Login");
            }
            else
            {
                return View("Account", obj);
            }
        }

        public ActionResult Account(Customer obj)
        {
            if(obj.isLoggedIn)
            {
                return View("Account", obj);
            }
            else
            {
                return View("Home");
            }
        }

        public ActionResult Login()
        {
            return View("Login");
        }

        public ActionResult Logout()
        {
            return View("Home");
        }

        public ActionResult DeleteUser(Customer obj)
        {
            bool success = obj.DeleteMe();
            if (success)
            {
                return View("Home");
            }
            else
            {
                return View("Account", obj);
            }
        }

        [HttpPost]
        public ActionResult UpdateUser(Customer obj)
        {
            obj.UpdateInfo(obj.Email, obj.CustomerName, obj.Notes);
            return View("Account", obj);
        }
    }
}